﻿using Vivan.Notifications.Validations;
using System;

namespace Vivan.Domain.Entities.Base
{
    public abstract class Entity<TKey> : Validatable, IEquatable<Entity<TKey>>
    {
        public TKey Id { get; set; }

        public virtual bool Equals(Entity<TKey> other)
        {
            if (Id == null || other.Id == null)
                return false;

            return Id.Equals(other.Id);
        }
    }
}
