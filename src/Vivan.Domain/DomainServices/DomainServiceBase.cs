﻿using Vivan.Notifications.Interfaces;
using Vivan.Notifications.Validations;

namespace Vivan.Domain.DomainServices
{
    public abstract class DomainServiceBase : Validatable
    {
        private readonly INotificationContext _notificationContext;

        public DomainServiceBase(INotificationContext notificationContext)
        {
            _notificationContext = notificationContext;
        }

        public override bool Validate()
        {
            return IsValid;
        }
    }
}
